# 雷劈网微信编辑器

1. 该软件已被驰骋公司收购，并接管维护。
2. 雷劈网站：http://wxeditor.leipi.org.cn/
3. 驰骋低代码开发平台：https://ccfast.cc

## 关于驰骋公司

1. 驰骋公司是研发开源国产化工作流引擎，表单引擎的公司。
2. 开源的.net版本的ccflow与java版本的jflow都采用雷劈表单设计器与流程设计器。


## 驰骋工作流引擎，表单引擎.

1. 驰骋公司官网：http://ccflow.org
2. 驰骋BPM：https://gitee.com/opencc
3. 相关产品下载：http://ccflow.org/downLP.htm

![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/120105_58cfb26d_8949271.png "雷劈网微信编辑器.png")

 

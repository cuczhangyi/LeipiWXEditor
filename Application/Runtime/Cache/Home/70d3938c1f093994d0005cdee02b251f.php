<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
 <head>
    
    <title>下载 Ueditor FormDesign Plugins -leipi.org</title>
    <meta name="keyword" content="ueditor formdesign plugins,ueditor扩展,web表单设计器,高级表单设计器,Leipi Form Design,web form设计器,web form designer">
    <meta name="description" content="提供多个表单设计器版本下载：1. 官网示例应用 [ PHP ] 2. 表单设计器[ 免安装 ] . 表单设计器 [ 仅扩展 ]">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="author" content="leipi.org">
    <link href="/Public/css/bootstrap/css/bootstrap.css?<?php echo ($_cjv); ?>" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="/Public/css/bootstrap/css/bootstrap-ie6.css?<?php echo ($_cjv); ?>">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="/Public/css/bootstrap/css/ie.css?<?php echo ($_cjv); ?>">
    <![endif]-->
    <link href="/Public/css/site.css?<?php echo ($_cjv); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var _root='<?php echo ($_site_url); echo U('/');?>',_controller = '<?php echo ($_controller); ?>';
    </script>
    

 </head>
<body>

<!-- fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="http://www.leipi.org" target="_blank">雷劈网</a>
      <div class="nav-collapse collapse">
        <ul class="nav">
            <li><a href="http://flowdesign.leipi.org">流程设计器</a></li>
            <li><a href="http://formdesign.leipi.org">表单设计器</a></li>
            <li><a href="http://formbuild.leipi.org">拖拽式表单设计器</a></li>
            <li><a href="http://qrcode.leipi.org">自动生成二维码</a></li>
			<li <?php if($_controller == 'index'): ?>class="active"<?php endif; ?>><a href="/">微信编辑器</a></li>
            <li <?php if($_controller == 'downloads'): ?>class="active"<?php endif; ?>><a href="<?php echo U('/downloads');?>">下载</a></li>
            <li><a href="http://bbs.leipi.org/" target="_blank">交流论坛</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>


	


<!-- Docs page layout -->
<div class="bs-header" id="content">
  <div class="container">

    <h1>下载</h1>
    <p>
        欢迎下载微信编辑器学习与交流
        <br/>
        交流Q群：<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=9f841437d1d7f1b57758d8cfc097f8be5e791ec97d9e9e81020433cfbe423a77"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="WEB 表单设计器" title="点击加入 WEB 表单设计器"></a>  143263697
    </p>
    
  </div>
</div>

<div class="container">
 
<div class="row">
<script type="text/javascript">
    /*表单设计器 下载 960*22 创建于 2014-09-23*/
    var cpro_id = "u1730744";
</script>
<script src="http://cpro.baidustatic.com/cpro/ui/c.js" type="text/javascript"></script>
<hr/>
</div>



<div class="row" >

<h2>PHP版本</h2>
<hr/>
 <h2>wxEditor.1.0.0 <small>稳定版 2015 / 05 / 17</small></h2>
    <table class="table table-hover">
        <tr>
            <th width="10%">文件</th>
			<th width="15%">描述</th>
            <th width="10%">下载</th>
        </tr>
        <tr>
            <td>1.wxEditor</td>
			<td>使用Thinkphp3.2框架 （需要PHP5.3以上运行环境）</td>
            <td>
				<img src="/Public/images/download.png" width="40">&nbsp;&nbsp;<a href="javascript:void(0);" id="downloads">>>> 进入下载地址列表</a>
            </td>
        </tr>
    </table>
<div class="alert">
参考:<a href="http://qrcode.leipi.org/doc.html#install" target="_blank">安装方法</a>
</div>
</div><!--end row-->





</div><!--end container-->


 

 <!-- Modal -->
<div id="alertModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>下载列表</h3>
  </div>
  <div class="modal-body">

	<div class="modal-ad" style="margin-left:30px;">
		<script type="text/javascript">
		/*表单设计下载列表 468*60 创建于 2015-01-05*/
		var cpro_id = "u1891982";
		</script>
		<script src="http://cpro.baidustatic.com/cpro/ui/c.js" type="text/javascript"></script>
	</div>
    <p class="modal-content">内容</p>
	<hr/>
	<p class="modal-downloads">
		<strong>下载：</strong><img src="/Public/images/download.png" width="30">&nbsp;<a href="http://pan.baidu.com/share/link?shareid=4001576577&uk=3543351756" target="_blank">百度网盘</a> | 提取密码：<code>****</code> <span class="ad">广告时间 <b>15</b> 秒，望支持 >>> </span>
		<a href="javascript:void(0);" target="_blank" class="mda">点击马上显示</a>
		<br/>
		<p class="modal-github hide">或者：<a href="https://github.com/payonesmile/formdesign" target="_blank">Github</a></p>
	</p>
	<hr/>
	<p>
		声明：<br/>
		本站仅分享开发思路和示例代码并且乐于交流和促进网络良性发展，是非商业工具，如有疑问请加群或邮件告知，积极配合调整。
	</p>
	<hr/>
	<p>
		关于：<br/>
		如有疑问可以加交流Q群：143263697，或发邮件至：payonesmile@qq.com
	</p>
	

  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">关闭</button>
  </div>
</div>


    
 <div class="bs-footer" role="contentinfo" id="bs-footer">
      <div class="container">
          <p><span class="glyphicon glyphicon-list-alt"></span> 免责声明：本站仅分享开发思路和示例代码并且乐于交流和促进网络良性发展，是非商业工具，如有疑问请加群或邮件告知，积极配合调整。</p>
          <p><span class="glyphicon glyphicon-list-alt"></span> 反馈：payonesmile@qq.com、<a target="_blank" style="color:#f30" href="http://bbs.leipi.org/"></p>

          <p><img src="/Public/images/alipay.png" style="height:100px" alt="支付宝扫一扫"  title="有你们的支持，我们将做的更好"/>
            支持 陈毅锋：支付宝扫一扫，<a href="<?php echo U('/support');?>"  title="有你们的支持，我们将做的更好">查看记录</a></p>

          <p><span class="glyphicon glyphicon-bookmark"></span> 鸣谢：
			<a href="http://pro.wwei.cn/wxeditor.html" target="_balnk" title="微信文案">微信文案</a>、
			<a href="http://www.leipi.org" target="_balnk">雷劈网</a>、
            <a href="http://git.leipi.org" target="_balnk">GitHub</a>、
            <a href="http://www.wwei.cn/" target="_balnk" title="二维码生成器">二维码生成器</a>、
            <a href="http://ck.leipi.org/" target="_balnk" title="网页播放器">网页播放器</a>

          </p>
          <p><a href="http://www.leipi.org" title="雷劈网"><img src="/Public/images/leipi.png" alt="雷劈认证 icon" style="height:30px"></a> &copy;2014 wxEditor V1.0.0 leipi.org <a href="http://www.miitbeian.gov.cn/" target="_blank">粤ICP备13051130号</a></p>
      </div>
</div>
    
<script src="/Public/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/Public/css/bootstrap/js/bootstrap.min.js"  type="text/javascript"></script>

<script type="text/javascript">



$(function(){
	var alertModal = $('#alertModal');
	//消息提示
	var mAlert = function(messages,fn)
	{ 
		if(!messages) messages = "";
		alertModal.find(".modal-content").html(messages);
		alertModal.modal('toggle');
		if(fn){fn();}//加载完成执行
	},gcc = function(t,p)
	{
		var cc = '';
		for (i=0;i<p.length ;i++ ) 
		{
				cc += "" + t[p[i]];
		} 
		return cc;
	}
	$("#downloads").on("click",function(){
		mAlert('',function(){
			//消息提示 38u8
			var smax = 15
			,sarr = new Array('#FF0033','#CC33FF','#CC3300','#00CCFF','#0000FF','#00CC00','#33FFFF','#99FF33','#CC0033','#FFCC33','#FF0033')
			,nc=$(".modal-downloads .ad")
			,nb=$(".modal-downloads b")
			,na=$(".modal-downloads .mda")
			,evalgcc = function(){$(".modal-downloads code").text(gcc(Array('a','b','c','38','c','tb','b2','u','d','e','9x','k1','8','y','4','op','h','b','c','z','ty','po','23','15'),Array(3,7,12)));$(".modal-github").show();nc.hide();na.hide()}
			na.on("click",function(){
				evalgcc();
				$(this).attr("href","http://formdesign.leipi.org/mall.html");
				return true;
			});
			setInterval(function(){
				if(smax>0){nc.css({"color":sarr[smax]})} smax--;
				if(nb.text()>0){nb.text(nb.text()-1)}
				},1000);
			setTimeout(evalgcc,15000);
		});
	});
})

</script>

<div style="display: none;">                                                                   
88888888888  88                             ad88  88                ad88888ba   8888888888   
88           ""                            d8"    88               d8"     "88  88           
88                                         88     88               8P       88  88  ____     
88aaaaa      88  8b,dPPYba,   ,adPPYba,  MM88MMM  88  8b       d8  Y8,    ,d88  88a8PPPP8b,  
88"""""      88  88P'   "Y8  a8P_____88    88     88  `8b     d8'   "PPPPPP"88  PP"     `8b  
88           88  88          8PP"""""""    88     88   `8b   d8'            8P           d8  
88           88  88          "8b,   ,aa    88     88    `8b,d8'    8b,    a8P   Y8a     a8P  
88           88  88           `"Ybbd8"'    88     88      Y88'     `"Y8888P'     "Y88888P"   
                                                          d8'                                
2014-3-15 Firefly95、Ard、xinG、Xiaoyaodaya               d8'  
</div>
<div style="display:none;">
  <script type="text/javascript">
  var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
  document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F1e6fd3a46a5046661159c6bf55aad1cf' type='text/javascript'%3E%3C/script%3E"));
  </script>
</div>
</body>
</html>